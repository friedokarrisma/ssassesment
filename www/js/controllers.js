angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope) {})

.controller('ChatsCtrl', function($scope, Chats) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  };
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
})

.controller('swapiCtrl', function($scope, $http){
  $scope.data     = null;
  $scope.dataform = {};
  $scope.execute = function(){
    if($scope.dataform.choice == '1'){
      $scope.url = 'http://swapi.co/api/people/1';
    }else if($scope.dataform.choice == '2'){
      $scope.url = 'http://swapi.co/api/people/2';
    }else if($scope.dataform.choice == '3'){
      $scope.url = 'http://swapi.co/api/people/3';
    }else if($scope.dataform.choice == '4'){
      $scope.url = 'http://swapi.co/api/people/4';
    }else{
      $scope.url = 'http://swapi.co/api/people/5';
    };
    $http({
      method: 'GET',
      url: $scope.url
    }).then(function successCallback(response) {
      console.log(JSON.stringify(response));
      $scope.name       = JSON.stringify(response.data.name);
      $scope.height     = JSON.stringify(response.data.height);
      $scope.mass       = JSON.stringify(response.data.mass);
      $scope.hair_color = JSON.stringify(response.data.hair_color);
      $scope.skin_color = JSON.stringify(response.data.skin_color);
      $scope.eye_color  = JSON.stringify(response.data.eye_color);
    }, function errorCallback(response) {
      $scope.error = JSON.stringify(response);
    });
  }
});
